---
author: LMCIEL
title: Plan de formation "Produits électroniques" - 1BCIEL
icon: fontawesome/solid/microchip
---

# :fontawesome-solid-microchip: Plan de formation "Produits électroniques" - 1BCIEL

#### :fontawesome-solid-microchip: - Ressource n°1 - Electronique
---

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_8c702ccad239deba7f133c9ab0f59b10.png)

test `#!js var test = 0;` fin du test

---

## :loudspeaker: - A l'ordre du jour

??? note "Sommaire"
	- 
	- 
	-


---

## <div style="color: #fff;background-color: #FBA61B"> - Lien cool </div>

- Lien STM32
[Ressources STM32](https://stm32python.gitlab.io/fr/){ .md-button target="_blank" rel="noopener" }
