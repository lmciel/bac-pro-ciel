---
author: LMCIEL
title: 🚀 Réalisations et maintenance de produits électronique
---

!!! abstract "Présentation Résumé"

 Exemple de tableau en Markdown : 

|Contenus|Capacités attendues|
|:--- | :--- |
|Identité numérique, e-réputation, identification, authentification|Connaître les principaux concepts liés à l'usage des réseaux sociaux.|
|Réseaux sociaux existants|Distinguer plusieurs réseaux sociaux selon leurs caracteristiques, y compris un ordre de grandeur de leurs nombres d'abonnés. Paramétrer des abonnements pour assurer la confidentialité de données personnelles.|
|Modèle économique des réseaux sociaux|Identifier les sources de revenus des entreprises de réseautage social.|
|Rayon, diamètre et centre d'un graphe|Déterminer ces caractéristiques sur des graphes simples.|
|Notion de « petit monde ». Expérience de Milgram|Décrire comment l'information présentée par les réseaux sociaux est conditionnée par le choix préalable de ses amis.|
|Cyberviolence|Connaître les dispositions de l'article 222-33-2-2 du code pénal. Connaître les différentes formes de cyberviolence (harcèlement, discrimination, sexting...) et les ressources disponibles pour lutter contre la cyberviolence.|

    




