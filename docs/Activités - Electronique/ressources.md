---
author: LMCIEL
title: 📚 Ressources générales
---

# Réalisation et maintenance de produits électronique

## Introduction à l'électronique

!!! abstract "Présentation"

    



??? note "01. Introduction à CodiMD"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01 - Introduction aux réseaux sociaux - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 01. Introduction à l'écriture collaborative dans CodiMD en Markdown](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Exposés : les réseaux sociaux

!!! abstract "Présentation"

    Dans cette activité, on propose aux élèves de préparer des exposés concernant les réseaux sociaux comme Twitter, Snapchat ou TikTok par exemple.



??? note "02. Exposés : les réseaux sociaux"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02 - Exposés - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


