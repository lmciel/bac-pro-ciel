---
author: LMCIEL
title: 💻 Mise en oeuvre des réseaux informatiques
---

!!! abstract "Présentation"

    Grâce à sa souplesse et à son universalité, internet est devenu le moyen de communication principal entre les hommes et avec les machines. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Protocole TCP/IP : paquets, routage des paquets| Distinguer le rôle des protocoles IP et TCP. Caractériser les principes du routage et ses limites. Distinguer la fiabilité de transmission et l'absence de garantie temporelle.|
|Adresses symboliques et serveurs DNS|Sur des exemples réels, retrouver une adresse IP à partir d'une adresse symbolique et inversement.|
|Réseaux pair-à-pair|Décrire l'intérêt des réseaux pair-à-pair ainsi que les usages illicites qu'on peut en faire.|
|Indépendance d'internet par rapport au réseau physique|Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non. Caractériser l'ordre de grandeur du trafic de données sur internet et son évolution.|