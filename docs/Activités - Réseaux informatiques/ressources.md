---
author: LMCIEL
title: 📚 Ressources générales
---

# Internet

## Introduction à internet

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent le thème Internet à travers une vidéo tirée du Mooc SNT. Puis l’enseignant.e présente les premières notions sur internet en introduisant la notion d’adresse IP simplement.



??? note "01. Introduction à internet"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction à internet - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 01. Introduction à internet](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Repères historiques

!!! abstract "Présentation"

    Dans cette activité, on présente quelques navigateurs classiques et une introduction aux langages HTML et CSS.
    Les élèves observeront et manipuleront des parties de code.


??? note "02. Repères historiques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Repères historiques - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[02. Repères historiques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

## Derrière la prise - baie de brassage

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent le réseau local de leur établissement et rencontrent la personne qui s’occupe du réseau. Cela va de la salle informatique jusqu’à la baie de brassage.


??? note "03. Derrière la prise - baie de brassage"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. Derrière la prise - baie de brassage - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[03. Derrière la prise - baie de brassage](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Routage

!!! abstract "Présentation"

    Dans cette activité, on met en place le principe du routage sous une forme débranchée à partir d’une étude de documents portant sur l’envoi et le cheminement d’une lettre de Boston jusqu’à Montauban.


??? note "04. Routage"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. Routage - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[04. Routage](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/04){ .md-button target="_blank" rel="noopener" }

## Routage des paquets

!!! abstract "Présentation"

    Dans cette activité, on aborde les grandes idées du routage des paquets dans un réseau. On présente en particulier deux éléments physiques importants : le switch et le routeur et on montre comment les adresses IP sont utilisées pour l’acheminement des paquets.


??? note "05. Routage des paquets"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. Routage des paquets - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[05. Routage des paquets](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/05){ .md-button target="_blank" rel="noopener" }

## Introduction aux protocoles TCP et IP

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e présente les protocoles TCP et IP aux élèves.


??? note "06. Introduction aux protocoles TCP et IP"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. Introduction aux protocoles TCP et IP - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[06. Introduction aux protocoles TCP et IP](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/06){ .md-button target="_blank" rel="noopener" }

## Notion de protocole IP

!!! abstract "Présentation"

    Dans cette activité, on met en place la notion de protocole IP sous une forme débranchée. On s’intéresse uniquement au parcours d’un message entre deux routeurs en respectant les tables de routages.


??? note "07. Notion de protocole IP"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Notion de protocole IP - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[07. Notion de protocole IP](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/07){ .md-button target="_blank" rel="noopener" }

## Notion de protocole TCP 

!!! abstract "Présentation"

    Dans cette activité, on met en place la notion de protocole IP sous une forme débranchée. L’enseignant.e s’intéresse uniquement au parcours d’un message entre deux routeurs en respectant les tables de routages.


??? note "08. Notion de protocole TCP"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Notion de protocole TCP - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[08. Notion de protocole TCP](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/08){ .md-button target="_blank" rel="noopener" }



## Introduction aux serveurs DNS

!!! abstract "Présentation"

    Dans cette activité, on présente la notion de DNS. On montre en particulier qu’à chaque adresse symbolique correspond une adresse IP et vice-versa.


??? note "09. Introduction aux serveurs DNS"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. Introduction au serveur DNS - INTERNET.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[09. Introduction aux serveurs DNS](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Internet/a_telecharger/09){ .md-button target="_blank" rel="noopener" }

