---
author: Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux, Mireille Coilhac, Pierre Marquestaut
title: HTML et CSS
---
 

## I. Présentation

### Modèle client-serveur



!!! abstract "Contenu et style"

    &#128083; Une page **HTML** est écrite en caractères lisibles mais codée.  
    Il n'est pas question d'étudier l'ensemble des possibilités du langage HTML, seulement les bases.

    &#128087;&#128086; / &#128221; On sépare le **style**(mise en page, couleurs, taille, type de polices de caractère, couleur de fond de page, ou des blocs,
    etc... du **contenu**.  
    &#128073; Le style est généralement défini dans un fichier séparé (ce n'est pas obligatoire, mais c'est très recommandé, et c'est ainsi que nous allons travailler).

    &#127797; Encore faut-il bien comprendre ce qu'on entend par le style et le contenu ! 
   


## II. Structure générale d'une page HTML

!!! abstract "Un début"

    ```html 
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
        </body>
    </html>
    ```


!!! abstract "Structure"

    Un document **HTML** est constitué d'un **document** contenant une **en-tête**, suivi d'un **corps**. 

    Repérer les balises permettant de délimiter :  

    * Le document

    ??? success "Solution"

        ```html
        <html>
        </html>
        ```

    * L'en-tête

    ??? success "Solution"

        ```html
        <head>
        </head>
        ```

    * Le corps

    ??? success "Solution"

        ```html
        <body>
        </body>
        ```



## III. Le concept de balises ouvrantes et fermantes, ou orphelines

### 1. Balises en paires

!!! abstract "Blocs"

    Comme vous pouvez commencer à le voir, dans l'exemple ci-dessus, une page <b>html</b> est organisée en blocs, 
    imbriqués les uns dans les autres. Le <b class="blue">header</b> est inclus dans le <b class="blue">html</b >, et le <b class="blue">title</b> est inclus dans
    le <b class="blue">header</b>


    Les blocs sont délimités par deux **balises**.  
    Par exemple :
    
    * **`<head>`** indique le début du header. C'est une **balise ouvrante**.
    * **`</head>`** indique la fin du header. C'est une **balise fermante**.


!!! abstract "Paires"

    On parle ici de balises en paire, avec une <b>balise ouvrante</b> et une <b>balise fermante</b>. 

!!! warning "&#128546; Et si on oublie une balise ?"

    
    Les navigateurs sont en général très tolérants, et une balise incorrecte (par exemple, il manque une fermante, ou il y a une fermante mais pas d'ouvrante...)
    sera ignorée. Dans les cas des balises ouvrantes sans fermante, cela engendrera le plus souvent un affichage incorrect et peu lisible. Dans
    le cas des balises fermantes sans ouvrante, l'effet est en général simplement nul : le navigateur ignore cette balise qui n'est 
    pas compréhensible. Toutefois, certains navigateurs, ou certaines balises, provoqueront des affichages totalement différents
    de ce qui est attendu et très souvent illisibles. C'est la responsabilité de l'auteur du document de veiller à respecter la syntaxe 
    du html.

??? tip "Astuce"

    &#128161; Il est conseillé, lorsqu'on écrit une balise ouvrante, d'instantanément écrire la balise fermante correspondante, et d'écrire ce que l'on souhaite entre les deux.

    😊 Pour s'y retrouver, il est recommandé d'utiliser des indentations.

!!! danger "Important"

    &#128073; Toutes ces balises indiquent **seulement** ce que contiennent les blocs. 
    Cela ne précise en aucun cas comment il faut les afficher dans la page. 
    Toutes les indications concernant **le style** seront
    fournies dans un fichier à part : la feuille de style **CSS**

???+ question 
    A partir des indications ci-dessus, <strong>compléter </strong>le schéma suivant :
    <iframe src="https://app.Lumi.education/api/v1/run/_o2qiV/embed" width="100%" height="330" frameborder="0" allowfullscreen="allowfullscreen"></iframe>


### 2. Balises orphelines

Certaines balises ne servent pas à désigner un contenu, mais servent à ajouter un élément.

!!! example ""Exemples"

    * <b class="blue">&lt;br></b> indique un saut de ligne.  
    * La balise <b class="blue">&lt;hr></b> introduit une ligne horizontale dans toute la largeur de la page.  
    * <b class="blue">La balise &lt;img></b> qui sert à insérer une image est également une <b>orpheline</b>, mais 
    son utilisation est un peu différente et nous en reparlerons.


😊 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." est bien écrite avec un saut de ligne grâce à la balise `<br>`


???+ question 

    <iframe src="https://app.Lumi.education/api/v1/run/0Tbh8Q/embed" width="750" height="380" frameborder="0" allowfullscreen="allowfullscreen">
    </iframe>

## IV. Ecrire et visualiser une page écrite en HTML

!!! info "Editeur de code HTML"

    Il existe de nombreux éditeurs de code HTML, comme Notepad++, ou Sublime Text. Ils vous aideront à ne pas faire de faute de syntaxe.

???+ question "Créer un fichier HTML"

    Ouvrir l'éditeur dont vous disposez, copiez-collez le code ci-dessous, puis enregistrez votre fichier sous le nom : `essai_HTML.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Apprendre</title>
        </head>


        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```



!!! info "Visualiser du code HTML"


    * 👉 A partir de maintenant nous allons tout simplement ouvrir un fichier HTML avec un navigateur.


???+ question "Visualiser un fichier HTML"

    * Utiliser votre explorateur de fichier, pour retrouver votre fichier.
    * Faire un clic droit sur votre fichier, puis "ouvrir avec", puis sélectionner "Firefox", ou "Google Chrome", ou "Microsoft Edge" par exemple.
    * Faire de même avec "Internet explorer" s'il est encore installé sur votre ordinateur.

    Que se passe-t-il ?

    ??? success "Vous pouvez obtenir quelque chose qui ressemble à ceci :"

        * Avec un navigateur "récent" : 

        ![avec Firefox](images/firefox.png){ width=35% .center}

        * Avec Internet Explorer : 

        ![avec Internet Explorer](images/intern_explor.png){ width=35% .center}


!!! info "Importance du `<head>`"

    😥 Comme vous le voyez, les navigateurs n'affichent pas toujours la même chose. C'est pourtant le même ficher, mais chaque navigateur peut interpréter les choses à sa façon.
    
    Ici c'est un problème d'encodage des accents qui provoque cette différence, et pour assurer un affichage correct dans tous les navigateurs, il faut ajouter une information dans le head.

    👉 Nous ajouterons dorénavant toujours dans `<head>` cette ligne : `<meta charset="UTF-8">`.

    😊 Ceci nous évitera le problème des accents, nous indiquons au navigateur que le texte est codé dans la norme utf-8 qui permet d'intégrer des accents dans le texte.


???+ question "Visualiser un fichier HTML avec le `head` complété"

    Recommencer l'expérience précédante avec le code suivant : 

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Apprendre</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```


## V. Un site à plusieurs pages

Nous allons voir comment cela fonctionne avec un site composé d'une page d'accueil et de deux autres pages.

!!! info "index"

    Il faut commencer par créer la page **obligatoirement** nommée `index.html`

    Voici un exemple plus détaillé que ce que nous avons déjà étudié.  
    Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `index.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <meta charset="utf-8">
                <title> Home </title>
            </head>
            <body>
                <header>
                    <h1> Mon en-tête : Accueil </h1>
                    <nav> 
                        <a href="page1.html"> page 1</a>
                        <br>
                        <a href="page2.html"> page 2</a>
                    </nav>
                </header>
                <main>
                    <section>
                    <h2> Section 1 </h2>
                    <p> Bla bla bla </p>
                    </section>

                    <section>
                    <h2> Section 2 </h2>
                    <p> Blo blo blo </p>
                    </section>
                </main>
                <footer> 
                    Mon pied de page
                </footer>
            </body>
        </html>
        ```


!!! info "Page 1"

    Vous devez maintenant écrire votre page 1.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page1.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 1</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 1</p>       
        </body>
        </html>
        ```


!!! info "Page 2 "

    Vous devez maintenant écrire votre page 2.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page2.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 2</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 2</p>       
        </body>
        </html>
        ```

!!! info "Visualiser votre tout petit site"

    Les trois fichiers doivent se trouver dans le même dossier.  
    Ouvrir le fichier `index.html` avec votre navigateur.



## VI. Le fichier CSS : principe général

!!! info

	Un document **html** est presque toujours accompagné d'au moins un fichier **CSS** qui décrit la mise en page,
	les formats (polices, couleurs, styles des éléments...). Un même fichier **CSS** peut être utilisé dans plusieurs pages d'un
	même site, ce qui permet, en le modifiant, de changer la présentation de toutes les pages (sinon il faudrait les modifier
	une à une). Mais ceci n'est qu'un aspect de l'intérêt des feuilles de styles. 

	👉 Essentiellement, elles ont pour rôle de permettre
	de séparer le contenu (qui est dans le html) de la forme (qui est dans le css)
	


### Sélecteur / Propriété / Valeur


Le CSS permet d'appliquer des styles sur différents éléments sélectionnés dans un document HTML. Par exemple, 
on peut sélectionner tous les éléments d'une page HTML qui sont paragraphes et afficher leurs textes en rouge avec 
ce code CSS :

!!! example "Exemple"

	```html
	p {
  		color: red;
	}
	```

!!! info

	Il y a 3 éléments dans la syntaxe :

	* **`p`** est le **sélecteur** : indique à quels éléments doit on applique le style. Ici, ce seront tout les éléments **`p`**, donc tous les paragraphes.
	* **`color`**est la **propriété** : indique (pour les éléments sélectionnés), quelle propriété on veut modifier.
	* **`red`** est la **valeur** qu'on attribue à la propriété pour les éléments sélectionnés.

???+ question 
    A partir des indications ci-dessus, <strong>compléter </strong>le schéma suivant :
    <iframe src="https://app.Lumi.education/api/v1/run/kBLSPL/embed" width="100%" height="330" frameborder="0" allowfullscreen="allowfullscreen"></iframe>



!!! note "Sélecteurs"
    Le sélecteur permet de sélectionner soit les éléments de même balise, les éléments partageant une même classe, ou un élément unique repéré par son identifiant.


???+ question 
    A partir des indications ci-dessus, <strong>compléter </strong>le schéma suivant :
    <iframe src="https://app.Lumi.education/api/v1/run/uFoq-c/embed" width="100%" height="330" frameborder="0" allowfullscreen="allowfullscreen"></iframe>


### Où mettre le code CSS ?

!!! abstract "En bref"

    Il est possible de l'inclure dans la page html mais il est recommandé de mettre le code CSS dans un autre fichier,
    ou même parfois dans plusieurs fichiers, chacun contenant des instructions de formatage spécifiques. 


### Comment inclure le CSS dans la page ?

!!! info "`<link>`"

	  Etant donné qu'on place les instructions de style dans un fichier annexe, il est nécessaire de l'indiquer dans la page html.  
	  On utilise pour cela l'instruction `<link>` entre `<head>` et `<\head>`

!!! abstract "Un exemple"

    ```html 
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="mon_styles.css" type="text/css">
        <title>Titre de l'onglet</title>
    </head>

    <body>

    </body>
    </html>
    ```


???+ note "Précisions"

    * **`rel=`** définit le type de relation, ici stylesheet indique un lien vers une feuille de style.
	* **`href=`** donne le nom du fichier, qui peut être un chemin absolu, relatif, ou une adresse web.
	* **`type=`** définit le type de contenu, pour les feuilles de styles, c'est text/css.</li>




