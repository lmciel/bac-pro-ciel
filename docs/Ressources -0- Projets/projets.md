---
tags: BAC PRO CIEL, 1BCIEL , Projet 
title: 1BCIEL - Projet vierge
description: 1BCIEL - Projet vierge
author: Equipe péda LP Louise Michel 
---

<!-- Ce document est un modéle à dupliquer en copiant collant le code dans une nouvelle note --> 


# <i class="fa fa-flag" aria-hidden="true"></i> - PROJET N° ... - Titre du projet


   

### <i class="fa fa-graduation-cap"></i> LP Louise Michel
<!-- 750*350 -->    
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_c6c1e66d30a228c4ee09d744531b23b3.png)



#  <i class="fa fa-users"></i> Classe
    
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_ed27a16627f62f6c782d423d47c2d1a8.png) 


###  <i class="fa fa-calendar"></i> Planning

* **Classe Groupe 1  du ..... au ..... 2024**
    
* **Classe Groupe 2  du ..... au ..... 2024**

* **Classe entière  du ..... au ..... 2024**
    

    
    
#### <i class="fa fa-bullseye"></i> **Titre du projet**
       
<!-- Insérer une photo du projet - taille recommandée 250*250 -->     
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_859a2cc67abd87371b4f54c739149e9b.png)




  
**Explication** du projet - [Liens](https://)
    
#### <i class="fa fa-users"></i> Déroulement
    
<!-- Compléter par les titres des différentes séances -->
    
- **Séance 0** - Mission du jour

    
:::spoiler On en est où ?
- [ ]**Séance 0** - .... 
- [ ]**Séance 1** - ....  
- [ ]**Séance 2** - ....  
- [ ]**Séance 3** - .... 
- [ ]**Séance 4** - .... 
- [ ]**Séance 5** - .... 
- [ ]**Séance 6** - .... 
- [ ]**Séance 7** - .... 
- [ ]**Séance 8** - .... 
- [ ]**Séance 9** - ....  
:::

- **A faire pour la prochaine séance :** Activités de préparation / Faire le parcours Moodle 
    
  
    
    
##### <i class="fa fa-cogs"></i> Activités

<!-- Sélectionner les activités prévues dans le projet et supprimer les autres --> 
 
<center>
    
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_984852b8638327aff40d30848ebb1aa9.png)

    
</center>

:::danger
:::spoiler <i class="fa fa-microchip"></i> - Réalisation et maintenance de produit électronique  

- [ ]**E1** - Etude et conception de produits électroniques
- [ ]**E2** – Tests et essais
- [ ]**E3** – Production et assemblage d’ensembles électroniques 
- [ ]**E4** – Intégration matérielle et logicielle 
- [ ]**E5** – Maintenance et réparation de produits électroniques
    
:::
  
:::success
:::spoiler <i class="fa fa-desktop "></i> - Mise en oeuvre de réseaux informatiques 

- [ ]**R1** – Accompagnement du client  
- [ ]**R2** – Installation et qualification 
- [ ]**R3** – Exploitation et maintien en condition opérationnelle 
- [ ]**R5** – Maintenance des réseaux informatiques
    

:::
    
:::info
:::spoiler <i class="fa fa-user-secret " ></i> - Valorisation de la donnée et cybersécurité 

- [ ]**D1** – Elaboration et appropriation d’un cahier des charges  
- [ ]**D2** – Développement et validation de solutions logicielles
- [ ]**D3** – Gestion d’incidents 
        
:::    
 
##### <i class="fa fa-graduation-cap"></i> Lien avec le référentiel
:::info   
:::spoiler Tableau croisé <i class="fa fa-table"></i>
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_fbac5772a44e9e32468cd129563dae7d.png)
:::    
:::spoiler Compétences
- [ ]**C01**-COMMUNIQUER EN SITUATION PROFESSIONNELLE (FRANÇAIS / ANGLAIS)

- [ ]**C03**-PARTICIPER A UN PROJET

- [ ]**C04**-ANALYSER UNE STRUCTURE MATERIELLE ET LOGICIELLE

- [ ]**C06**-VALIDER LA CONFORMITE D’UNE INSTALLATION

- [ ]**C07**-REALISER DES MAQUETTES ET PROTOTYPES

- [ ]**C08**-CODER

- [ ]**C09**-INSTALLER ELEMENTS ELECTRONIQUES OU INFORMATIQUES

- [ ]**C10**-EXPLOITER UN RESEAU INFORMATIQUE

- [ ]**C11**-MAINTENIR UN SYSTEME ELECTRONIQUE OU INFORMATIQUE
:::
    
:::spoiler Connaissances et niveaux de maîtrise  
	
- [ ]**C01**–Communication interpersonnelle <i class="fa fa-battery-half"></i>
- [ ]**C01**-Théorie de la communication <i class="fa fa-battery-half"></i>
- [ ]**C01**–Communication écrite	<i class="fa fa-battery-three-quarters"></i>
- [ ]**C01**–Communication orale <i class="fa fa-battery-three-quarters"></i>
- [ ]**C01**–Règles de présentation et typographie <i class="fa fa-battery-three-quarters"></i>

- [ ]**C03**–Outils de suivi <i class="fa fa-battery-half"></i>
- [ ]**C03**–Budgétisation humains et matériels <i class="fa fa-battery-half"></i>
- [ ]**C03**–Gestion de commandes <i class="fa fa-battery-three-quarters"></i>
- [ ]**C03**–Méthodologie de projet <i class="fa fa-battery-three-quarters"></i>

- [ ]**C04**–Acteurs de l’écosystème normatif … <i class="fa fa-battery-half"></i>
- [ ]**C04**–SysML <i class="fa fa-battery-half"></i>
- [ ]**C04**–Structures électroniques An et Dig <i class="fa fa-battery-half"></i>
- [ ]**C04**–Structures programmables <i class="fa fa-battery-half"></i>
- [ ]**C04**–Anglais technique <i class="fa fa-battery-half"></i>
- [ ]**C04**–Infrastructures matérielles et logicielles <i class="fa fa-battery-three-quarters"></i>
- [ ]**C04**–Documents d’architecture métiers (synop) <i class="fa fa-battery-three-quarters"></i>
- [ ]**C04**–Programmation en langage évolué <i class="fa fa-battery-three-quarters"></i>
- [ ]**C04**–Connaissances en électronique ana <i class="fa fa-battery-three-quarters"></i>

- [ ]**C06**–Principes des modèles en couches <i class="fa fa-battery-quarter"></i>
- [ ]**C06**–Architecture réseaux tertiaires et indus <i class="fa fa-battery-half"></i>
- [ ]**C06**–Structures matérielles (ana et dig) <i class="fa fa-battery-half"></i>
- [ ]**C06**–Structures programmables <i class="fa fa-battery-half"></i>
- [ ]**C06**–Réseaux info (protocoles, équipements…) <i class="fa fa-battery-three-quarters"></i>
- [ ]**C06**–Appareils de mesure <i class="fa fa-battery-three-quarters"></i>

- [ ]**C07**–Techno de frabrication PCB (indus) <i class="fa fa-battery-half"></i>
- [ ]**C07**–Procédés indus pose et brasure <i class="fa fa-battery-half"></i>
- [ ]**C07**–Normes IPC et QSE	<i class="fa fa-battery-half"></i>
- [ ]**C07**–Notions et concepts du dvlpt durable <i class="fa fa-battery-half"></i>
- [ ]**C07**–Techno composants CMS, traversant… <i class="fa fa-battery-three-quarters"></i>
- [ ]**C07**–Procédés de prototypage <i class="fa fa-battery-three-quarters"></i>
    
- [ ]**C08**–Langages de dev, IDE … <i class="fa fa-battery-half"></i>
- [ ]**C08**–Outils de modélisation <i class="fa fa-battery-half"></i>
- [ ]**C08**–Politiques internes applications <i class="fa fa-battery-half"></i>
- [ ]**C08**–Infrastructures matérielles et logicielles <i class="fa fa-battery-half"></i>
- [ ]**C08**–Prog (variables, if , boucles, fonctions) <i class="fa fa-battery-three-quarters"></i>
    
- [ ]**C09**–Modèles OSI IP <i class="fa fa-battery-quarter"></i>
- [ ]**C09**–IOT <i class="fa fa-battery-half"></i>
- [ ]**C09**–Serveur et ordinateur (windows, linux…) <i class="fa fa-battery-half"></i>
- [ ]**C09**–Architecture réseau et système <i class="fa fa-battery-half"></i>
- [ ]**C09**–Plan méca et archi 2D et 3D <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Schémas électriques, électro et réseaux <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Technos de raccordement filaire, optique..) <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Appareils de mesures <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Habilec B1V <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Outillage mécanique et spécifique <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Certification AIPR (proximité réseaux) <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Protocoles IPv4 <i class="fa fa-battery-three-quarters"></i>
- [ ]**C09**–Elements actifs <i class="fa fa-battery-three-quarters"></i>
    
- [ ]**C10**–Système exploitation Unix et win <i class="fa fa-battery-half"></i>
- [ ]**C10**–Bonnes pratiques en sécu info <i class="fa fa-battery-half"></i>
- [ ]**C10**–Lignes de commandes d’équipements <i class="fa fa-battery-three-quarters"></i>
- [ ]**C10**–Méthodes de connexion à distance <i class="fa fa-battery-three-quarters"></i>
    
- [ ]**C11**–Normes QSE <i class="fa fa-battery-quarter"></i>
- [ ]**C11**–Structures programmalbles <i class="fa fa-battery-half"></i>
- [ ]**C11**–Caractères signaux non complexes <i class="fa fa-battery-half"></i>
- [ ]**C11**–Formation Habilec BR <i class="fa fa-battery-half"></i>
- [ ]**C11**–Economie de la maintenance <i class="fa fa-battery-half"></i>
- [ ]**C11**–Types de maintenance <i class="fa fa-battery-half"></i>
- [ ]**C11**–Normes IPC <i class="fa fa-battery-half"></i>
- [ ]**C11**–Structures électroniques ana et dig <i class="fa fa-battery-three-quarters"></i>
- [ ]**C11**–Appareils de mesure (multi, oscillo…) <i class="fa fa-battery-three-quarters"></i>
::: 
:::info   
:::spoiler Légende des niveaux <i class="fa fa-battery-full"></i>
<i class="fa fa-battery-quarter"></i> Niveau 1 - niveau d’information
<i class="fa fa-battery-half"></i> Niveau 2 -  niveau d’expression 
<i class="fa fa-battery-three-quarters"></i> Niveau 3 -  niveau de la maîtrise d’outils
:::  

    
    
###### <i class="fa fa-tasks"></i> Tâches   
   
<center>

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_7e36063d59e100057d664d9203968f18.png)


</center>
    
:::danger
:::spoiler <i class="fa fa-microchip"></i> - Réalisation et maintenance de produit électronique

* E1-T1 : Analyse et saisie d’un schéma, d’une carte électronique (non complexe) ou étude d’un système électronique communicant à partir d’un cahier des charges
* E1-T2 : Placement et routage d’une carte électronique et génération des fichiers de fabrication
* E1-T3 : Réalisation d’un prototype et mise au point d’une carte électronique (non complexe)
* E1-T4 : Intégration dans son environnement à partir d’un cahier des charges
* E2-T1 : Tests et mesures nécessaires à la vérification d'une carte et/ou d’un système électronique communicant * E2-T2 : Mise en place d’un environnement de tests
* E2-T3 : Application d’un protocole de tests et de mesures
* E3-T1 : Préparation, assemblage et contrôle des cartes et/ou des sous-ensembles électroniques communicants au vu d’une installation
* E3-T2 : Configuration, paramétrage, et intégration des outils de production et/ou des équipements (matériels et logiciels) ainsi que le matériel de contrôle
* E3-T3 : Renseignement du suivi de production
* E3-T4 : Vérification de la conformité des caractéristiques de fonctionnement et intervention corrective si nécessaire 
* E4-T1 : Préparation et contrôle préalable
* E4-T2 : Intégration mécanique des sous-ensembles électronique, électrique, automatique, filaire et optique
* E4-T3 : Intégration des équipements électroniques communicants sur site
* E4-T4 : Installation et paramétrage des logiciels et des équipements communicants en fonction de protocoles (cahier des charges)
* E4-T5 : Vérification des caractéristiques de fonctionnement en conformité avec le projet
* E4-T6 : Conseils au client sur l'utilisation, le fonctionnement, l'entretien des équipements et de l’installation  
* E5-T1 : Identification des fonctions principales et secondaires constitutives d’une carte électronique (non complexe), d’un sous-système ou d’un système électronique communicant
* E5-T2 : Constat et identification du dysfonctionnement
* E5-T3 : Réalisation d’une opération de réparation ou de maintenance corrective ou préventive (sur site ou à distance)
* E5-T4 : Tests et vérification de la conformité
* E5-T5 : Renseignement de la fiche technique d’intervention et/ou du cahier de maintenance
* E5-T6 : Communication au client ou à la hiérarchie    
:::
:::success
:::spoiler <i class="fa fa-desktop "></i> - Mise en oeuvre de réseaux informatiques
 
* R1-T1 : Prise en compte des besoins du client
* R1-T2 : Réception de l’installation avec le client
* R1-T3 : Information ou conseil au client 
* R2-T1 : Prise en compte de la demande du client
* R2-T2 : Vérification du dossier, interprétation des plans d’exécution
* R2-T3 : Préparation du chantier en fonction de l’intervention souhaitée
* R2-T4 : Réalisation des opérations avec intégration des contraintes client et contrôle
* R2-T5 : Recettage de l’installation
* R3-T1 : Réalisation d’un diagnostic de premier niveau * R3-T2 : Configuration matérielle et logicielle des équipements 
* R3-T3 : Intégration de nouveaux équipements
* R3-T4 : Mise à jour des équipements 
* R5-T1 : Réalisation de diagnostics et d’interventions de maintenance curative
* R5-T2 : Réparation des liaisons, changement de cartes ou d’équipements
* R5-T3 : Rédaction de compte rendu d’intervention
    

:::    
:::info
:::spoiler <i class="fa fa-user-secret " ></i> - Valorisation de la donnée et cybersécurité
* D1–T1 : Collecte des informations
* D1–T2 : Analyse des informations
* D1–T3 : Interprétation d’un cahier des charges
* D1–T4 : Formalisation du cahier des charges 
* D2–T1 : Modélisation d’une solution logicielle
* D2–T2 : Développement, utilisation ou adaptation de composants logiciels
* D2–T3 : Tests et validation
* D3–T1 : Ouvrir et catégoriser les tickets par niveau de criticité
* D3–T2 : Traiter les tickets
* D3–T3 : Remédier aux incidents
* D3–T4 : Élaborer les rapports d’incidents
* D3–T5 : Transmettre l’information (escalade) 
:::

    
###### <i class="fa fa-pencil-square-o"></i> Documents ressources 
* [<i class="fa fa-external-link"></i> Ressources GitLab ](https://lmciel.forge.apps.education.fr/bac-pro-ciel/)
* [<i class="fa fa-file-pdf-o"></i> Fiche activité](https://)
* [<i class="fa fa-external-link"></i> Lien séquence Moodle](https://)
* [<i class="fa fa-book"></i> Ressources .H5P](https://bacprociel.dscloud.me:81/Nextcloud/index.php/s/XNLXneFjB599S67)
* [<i class="fa fa-folder-open"></i> Dossier de dépot des documents](https://) 





# <i class="fa fa-binoculars"></i> MISE EN PLACE DU PROJET - DÉFINITIONS DES OBJECTIFS



## De quoi parle-t-on ? <i class="fa  fa-bullhorn"></i>

# Emplacement 

##    
# <i class="fa fa-hand-o-down"></i> 

### <i class="fa  fa-hourglass-o"></i> Planning et durées
    
* **1BCIEL G1** du ... au ... 

    
* **2TNE** du ... au ... 

    
### Préparation pédagogique de la période de formation en milieu professionnel
    
La préparation pédagogique de la période de formation  en milieu professionnel concerne à la fois :

* **l'élève,** que toute l'équipe pédagogique doit aider à acquérir les savoirs, les savoir-faire et savoir-être nécessaires à une bonne intégration dans le milieu professionnel ; 

* **l'organisme d'accueil**, que l'établissement scolaire doit informer de la manière la plus exhaustive possible sur les caractéristiques de la formation suivie par l'élève et sur les objectifs de la période de formation  en milieu professionnel.

:::spoiler <span style="color: #3592CF;"> Pour en savoir plus <i class="fa  fa-info-circle"></i></span>
 
Le professeur référent définit avec le responsable de l'organisme d'accueil les modalités de déroulement du séjour en entreprise et les tâches qui seront confiées à l'élève. L'entreprise désigne de son côté un tuteur de stage.

Si la préparation de l'élève est nécessaire quelle que soit la période de formation en milieu professionnel, la préparation à la première période de formation en milieu professionnel revêt une importance toute particulière et doit faire l'objet d'une attention renforcée. Depuis la rentrée 2016, tous les élèves entrant en classe de seconde professionnelle ou en en première année de CAP bénéficient d'une semaine de préparation à leur première période de formation en milieu professionnel.

Ce temps, construit par l'équipe pédagogique et associant les partenaires du monde économique, est utilisé pour préparer l'élève aussi bien aux attendus du monde professionnel qu'aux règles de santé et de sécurité indispensables au bon déroulement de cette première prise de contact avec un environnement professionnel.

::: 

### Suivi de la période de formation en milieu professionnel 
    
L'accompagnement pendant la période de formation en milieu professionnel est assuré par un enseignant référent.
    
:::spoiler <span style="color: #3592CF;"> Les objectifs : <i class="fa  fa-info-circle"></i></span> 

* **s'assurer** du bon déroulement de ladite période ;

* **faire le point sur les activités** de l'élève et ses progrès ;
    
* **compléter ou rectifier le choix des activités** qui lui sont confiées en application de la convention de stage ;
    
* **réaliser les évaluations prévues** dans le règlement d'examen de certains diplômes.

:::

#### <i class="fa fa-pencil-square-o"></i> Références
[Textes de références](https://eduscol.education.fr/666/periodes-de-formation-en-milieu-professionnel-pfmp)
    
    
    


## La recherche des organismes <i class="fa fa-handshake-o" ></i> 

# par qui, comment ?

    

#### <i class="fa fa-user"></i> Pour l’enseignant



#### <i class="fa fa-user"></i> Pour l’élève
    

    
#### <i class="fa fa-user"></i> Pour les parents


    

#### <i class="fa fa-address-book" aria-hidden="true"></i> Carnet d'adresses
[Listing des entreprises partenaires](https://)
    
    


## Quels documents ? <i class="fa fa-pencil-square-o"></i>

 <i class="fa fa-laptop"></i> <i class="fa fa-comments-o"></i> <i class="fa fa-pencil-square-o"> </i>  <i class=" fa fa-check-square-o"></i> <i class="fa fa-briefcase"></i> <i class="fa fa-handshake-o"></i> <i class="fa fa-audio-description" aria-hidden="true"></i> <i class="fa fa-file-video-o" aria-hidden="true"></i> <i class="fa fa-code-fork"></i> <i class="fa fa-universal-access" aria-hidden="true"></i>
    
* [La préconvention](https://) à transmettre à ... ou à déposer dans le dossier de dépot ci-dessous.

* La convention à récupérer auprés de .... et à remettre en fin de PFMP
    
* L'attestation de stage à remettre en fin de PFMP
    
* La grille d'évaluation etc...

## Dossier de dépot des documents

[![](https://minio.apps.education.fr/codimd-prod/uploads/upload_84434f1139d24a0e56c5b112f263a2a4.png)Dossier de dépot](https://nuage03.apps.education.fr/index.php/s/DTbxs63rK34FGtk)



### <i class="fa fa-comments-o"></i> Le feedback immédiat
    
- .....

- .....
    


## Retour en centre de formation  <i class="fa fa-graduation-cap"></i> 

# Quoi, quand, comment ?

    

#### <i class="fa fa-user"></i> Pour l’enseignant



#### <i class="fa fa-user"></i> Pour l’élève
    

    
#### <i class="fa fa-user"></i> Pour les parents


    

#### <i class="fa fa-address-book" aria-hidden="true"></i> Carnet d'adresses
[Listing des entreprises partenaires](https://)
    
    






    
    


