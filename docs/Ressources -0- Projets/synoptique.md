---
tags: Tutoriel
---
# Faire des synoptiques

---

- [ ] Tâche 1 
- [ ] Tâche 1 
- [ ] Tâche 1 
- [ ] Tâche 1 
- [ ] Tâche 1 

https://lmciel.forge.apps.education.fr/bac-pro-ciel/images/logoBACPROCIEL.png

| header | header |
| ------ | ------ |
|  - [ ] Tâche 1       | |
|  - [ ] Tâche 1       |  |

```mermaid
%%{init: {"flowchart": {"htmlLabels": true},"theme": "forest", "securityLevel"="loose"}}%%

  graph TD
DIR("<img src='https://lmciel.forge.apps.education.fr/bac-pro-ciel/images/logoBACPROCIEL.png' width='30' /> BAC PRO CIEL")
A[Christmas] -->|Get money| B(Go shopping)
          B --> C{Let me think}
          B --> G[/Another/]
          C ==>|One| D[Laptop]
          C -->|Two| E[iPhone]
          C -->|"<img src='https://lmciel.forge.apps.education.fr/bac-pro-ciel/images/logoBACPROCIEL.png' width='30' /> BAC PRO CIEL"| F[fa:fa-car Car]
          subgraph section
            C
            D
            E
            F
            G
          end

```

<details><summary>Mermaid</summary>

```mermaid
flowchart LR
    id1[This is the text in the box]
```



```mermaid
flowchart LR
    id1[This is the text in the box]
```
---

 
```mermaid
flowchart TD
    Start --> Stop
```
---
```mermaid
flowchart LR
    Start --> Stop
```
---
```mermaid
flowchart LR
    id1(This is the text in the box)
```

---
```mermaid
    flowchart LR
    id1([This is the text in the box])
```
---
```mermaid
    flowchart LR
    id1[[This is the text in the box]]
```
---
```mermaid
    flowchart LR
    id1[(Database)]
```
---
```mermaid
   flowchart LR
    id1((This is the text in the circle))
```
---
```mermaid
   flowchart LR
    id1>This is the text in the box]
```
---
```mermaid
   flowchart LR
    id1{This is the text in the box}
```
---
```mermaid
   flowchart LR
    id1{{This is the text in the box}}
```
---

```mermaid
   flowchart TD
    B[\Go shopping/]
```

---

```mermaid
   flowchart LR
    A --> B
    C---D
    E-- This is the text! ---F
    G---|This is the text|H
    I-->|text|J
    K-.->L;
    M-. text .-> N
    O --o P
    Q --x R
    
```
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_bbce4134d37a78538575ea4e915dbe2d.png)

---
```mermaid
   flowchart TB
    c1-->a2
    subgraph one
    a1-->a2
    end
    subgraph two
    b1-->b2
    end
    subgraph three
    c1-->c2
    end
```
---

```mermaid
   flowchart TD
    A[Start] --> B{Is it?}
    B -- Yes --> C[OK]
    C --> D[Rethink]
    D --> B
    B -- No ----> E[End]
```
---
```mermaid
   flowchart TB
    c1-->a2
    subgraph ide1 [one]
    a1-->a2
    end
```
---
```mermaid

%%{init: {logLevel': 'debug', 'theme': 'forest'} }%%

  flowchart LR
    A-->B
    B-->C
    C-->D
    click A callback "Tooltip for a callback"
    click B "https://www.github.com" "This is a tooltip for a link"
    click C call callback() "Tooltip for a callback"
    click D href "https://www.github.com" "This is a tooltip for a link"
```
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_126e1c48b7480de5b600de3354e66d8a.png)

---

```mermaid
%%{init: {'logLevel': 'debug', 'theme': 'forest' } }%%
   flowchart LR
    id1(Start)-->id2(Stop)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#dae,stroke-width:3px,color:#fff,stroke-dasharray: 5 5
```

---
```mermaid
%%{init: {'logLevel': 'debug', 'theme': 'forest' } }%%
   flowchart LR
    A:::someclass --> B
    classDef someclass fill:#f96
```
---

```mermaid
%%{init: {'logLevel': 'debug', 'theme': 'forest' } }%%
   flowchart TD
    B["fab:fa-twitter for peace"]
    B-->C[fa:fa-ban forbidden]
    B-->D(fa:fa-spinner)
    B-->E(A fa:fa-camera-retro perhaps?)
```
---
```mermaid
%%{init: {'logLevel': 'debug', 'theme': 'forest' } }%%
   flowchart TD
    B["fab:fa-twitter for peace"]
    B-->C[fa:fa-ban forbidden]
    B-->D(fa:fa-spinner)
    B-->E((A fa:fa-camera-retro perhaps?))
```
---

```mermaid
%%{init: {'logLevel': 'debug', 'theme': 'forest' } }%%
journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

---

```mermaid
gantt
    title A Gantt Diagram
    dateFormat YYYY-MM-DD
    section Section
        A task          :a1, 2014-01-01, 30d
        Another task    :after a1, 20d
    section Another
        Task in Another :2014-01-12, 12d
        another task    :24d
```

---

```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

---
```mermaid
graph TB
    sq[Square shape] --> ci((Circle shape))

    subgraph A
        od>Odd shape]-- Two line<br/>edge comment --> ro
        di{Diamond with <br/> line break} -.-> ro(Rounded<br>square<br>shape)
        di==>ro2(Rounded square shape)
    end

    %% Notice that no text in shape are added here instead that is appended further down
    e --> od3>Really long text with linebreak<br>in an Odd shape]

    %% Comments after double percent signs
    e((Inner / circle<br>and some odd <br>special characters)) --> f(,.?!+-*ز)

    cyr[Cyrillic]-->cyr2((Circle shape Начало));

     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```
---

```mermaid
%%{init: {"flowchart": {"htmlLabels": true},"theme": "forest", "securityLevel"="loose"}}%%

  graph TD
DIR("<img src='https://lmciel.forge.apps.education.fr/bac-pro-ciel/images/logoBACPROCIEL.png' width='30' /> BAC PRO CIEL")
A[Christmas] -->|Get money| B(Go shopping)
          B --> C{Let me think}
          B --> G[/Another/]
          C ==>|One| D[Laptop]
          C -->|Two| E[iPhone]
          C -->|"<img src='https://lmciel.forge.apps.education.fr/bac-pro-ciel/images/logoBACPROCIEL.png' width='30' /> BAC PRO CIEL"| F[fa:fa-car Car]
          subgraph section
            C
            D
            E
            F
            G
          end

```
---

---

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#BB2528',
      'primaryTextColor': '#fff',
      'primaryBorderColor': '#7C0000',
      'lineColor': '#F8B229',
      'secondaryColor': '#006100',
      'tertiaryColor': '#fff'
    }
  }
}%%

        graph TD
          A[Christmas] -->|Get money| B(Go shopping)
          B --> C{Let me think}
          B --> G[/Another/]
          C ==>|One| D[Laptop]
          C -->|Two| E[iPhone]
          C -->|Three| F[fa:fa-car Car]
          subgraph section
            C
            D
            E
            F
            G
          end
```
---

```mermaid
%%{
  init: {
    'theme': 'neutral',
    'themeVariables': {
      'primaryColor': '#B60073',
      'primaryTextColor': '#fff',
      'primaryBorderColor': '#7C0000',
      'lineColor': '#0073B6',
      'secondaryColor': '#73B600',
      'tertiaryColor': '#fff'
    }
  }
}%%

        graph TD
          A[BAC PRO CIEL] -->|Pôle 1| B(Réalisation et maintenance de produits électronique)
          A[BAC PRO CIEL] -->|Pôle 2| C(Réalisation et maintenance de produits électronique)
          B --> D{Let me think}
          B --> E[/Another/]
          D ==>|One| D[Laptop]
          D -->|Two| E[iPhone]
          D-->|Three| F[fa:fa-car Car]
          subgraph section
            D
            E
            F
            G
          end
```
```mermaid

%%{init: {logLevel': 'debug', 'theme': 'forest'} }%%

  flowchart LR
    A-->B
    B-->C
    C-->D
    click A callback "Tooltip for a callback"
    click B "https://www.github.com" "This is a tooltip for a link"
    click C call callback() "Tooltip for a callback"
    click D href "https://www.github.com" "This is a tooltip for a link"
```
</details>
---



