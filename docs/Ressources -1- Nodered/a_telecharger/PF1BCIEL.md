
# Plan de formation 1BCIEL

#### :book: - Ressource n°1 - Les réseaux informatiques
---

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_8c702ccad239deba7f133c9ab0f59b10.png)


---

## :loudspeaker: - A l'ordre du jour

??? note "Sommaire"
	- Introduction à internet    
	- Repères historiques
	- Derrière la prise - baie de brassage
	- Routage
	- Routage des paquets
	- Introduction aux protocoles TCP et IP
	- Notion de protocole IP
    - Notion de protocole TCP
    - Introduction aux serveurs DNS

---

## <div style="color: #fff;background-color: #FBA61B"> :globe_with_meridians: - Introduction à internet </div>

- Dépot des ressources 
[01. Introduction](https://forge.apps.education.fr/lmciel/bac-pro-ciel/-/tree/main/docs/Donnees/){ .md-button target="_blank" rel="noopener" }
