---
author: LMCIEL
title: Initiation à NodeRED
---

# :fontawesome-solid-bug: NodeRED

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_76f51edcea03e3485ffb027e6bd31b27.png){: .center width="800"}

## Introduction

!!! abstract "Pour commencer"
	Régulièrement, de nouvelles rumeurs apparaissent sur différentes méthodes de programmation.
	Je ne citerais ici que les principales :

	* L'IA remplacera les codeurs
	* La nouvelle manière de coder, est sans code
	* Go1 remplacera la plupart des langages
	* Web Assembly est l'avenir de la programmation dans les navigateurs
	* …

La plupart de ces déclarations sont liées à un engouement pour de nouvelles technologies. Dans cette découverte, nous allons nous intéresser au codage sans code ou presque (No Code ou LowCode).

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_2f51cd257873859a83f0d7e4962926eb.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_b69311b1b87e2ace19a6e2f2a99b33b7.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_76e53f1d6de50969ad5ff94def9bb459.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_daee96a2bf45f7c3a46c29c9366f8dcf.png){: .center width="800"}

## 1 - NO CODE / LOW CODE

Depuis plusieurs années, on rencontre cette nouvelle méthode de programmation, dite graphique.
L'objectif de ces outils No Code est de permettre de transformer la phase de codage en phase de gestion de processus, pour simplifier le travail du développeur, mais surtout, diminuer les coûts de développement.

Pour cela, il faut utiliser des outils graphiques permettant de relier des cellules spécialisées dans
lesquelles on saisit le minimum d'informations.

Toutefois, ce genre de plateforme a également ses inconvénients : le code devient moins
accessible, le développeur est plus dépendant de l'outil.

Par exemple, en JavaScript, vous pouvez utiliser l'éditeur de votre choix, comme VSCode,
Notepad++, PHPStorm, NetBeans… ce n'est plus le cas en No Code.

Nous allons donc aborder de manière très légère ce concept, avec l'environnement Node-Red qui
est Low code (il implique encore de la programmation traditionnelle), afin d'avoir une culture et
une compréhension globale du fonctionnement.

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d9c2b26ceca902e55b1e548bd109b4c0.png){: .center width="800"}

<figure>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Zueq0D0P0JE?si=2WnJykkd-M_u2-o4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> 
<figcaption>DOC 1 : Tuto video NodeRED</figcaption>  
</figure>


??? note "01. Initiation à NodeRED"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/decouverte_de_la_programmation_low_code_node-red.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 01. Introduction au lowcode avec NodeRED](https://forge.apps.education.fr/lmciel/bac-pro-ciel/-/tree/main/docs/Ressources - Nodered/a_telecharger/01){ .center .md-button target="_blank" rel="noopener" }


## 2 - Utilisation dans les systémes IOT - Utilisation du dashboard dans NodeRED

!!! abstract "Présentation"

    La suite avec les liens MQTT dans le cadre d'un systéme IOT. A suivre ...




