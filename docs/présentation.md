---
hide:
  - toc        # Hide table of contents 
  - navigation
---

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_8c702ccad239deba7f133c9ab0f59b10.png){: .center width="800"}

# :fontawesome-solid-user-secret: CYBERSÉCURITÉ, :material-lan: INFORMATIQUE ET RÉSEAUX, :fontawesome-solid-microchip: ÉLECTRONIQUE 

??? info "Présentation"

    Le ou la titulaire du baccalauréat professionnel « Cybersécurité, Informatique et réseaux, Électronique » (CIEL) intervient dans des secteurs d’activités variés tels que la « silver économie », la domotique, l’électroménager, la cybersécurité, la réparation de produits électroniques, la télémédecine, mais aussi :

    * l’industrie (les automatismes industriels et « usine 4.0 et 5.0 », smart city etc.) ;
    * les transports ;
    * les services ;
    * l’automobile et plus largement les nouveaux moyens de déplacements ;
    * l'aéronautique, la défense, l'espace ;
    * les télécommunications ;
    * les sciences et technologies de l'information et de la communication, le multimédia ; 
    * le commerce des matériels électroniques et numériques ;
    * l’internet des objets (IoT) ;

    Il ou elle peut exercer son activité dans des entreprises de tailles variables allant des TPE aux grandes entreprises.


??? Abstract "Types d’emploi accessibles"

    Les emplois les plus couramment exercés par le ou la titulaire du baccalauréat professionnel « Cybersécurité, Informatique et réseaux, Électronique » couvrent les domaines de la réalisation, de la production, de l’intégration, de la maintenance de produits électroniques ainsi que la mise en œuvre de réseaux informatiques, la valorisation de la donnée et la cybersécurité.  
    
    On peut citer par exemple les emplois suivants : 

    * monteur-câbleur ou monteuse-câbleuse ; 
    * opérateur ou opératrice en production sur machine CMS ; 
    * technicien ou technicienne en design de cartes électroniques ;  
    * technicien ou technicienne de câblage et d’intégration d’équipements électroniques ; 
    * technicien ou technicienne de réparation d’équipements électroniques ; 
    * agent de contrôle et de montage en électronique ; 
    * agent de support technique client ; 
    * technicien ou technicienne d'installation (télécommunications et radio, réseaux informatiques, systèmes de sécurité, alarme et détection incendie, etc.) ; 
    * technicien ou technicienne de maintenance (réseaux câblés de communication en fibre optique, réseaux informatiques, systèmes d'alarme et de sécurité et de télésurveillance, système de téléphonie IP et salle de visio-conférence IP, etc.) ; 
    * technicien ou technicienne en télécommunications et réseaux d'entreprise.  


??? exemple "Référentiel du BAC PRO CIEL"

	<div class="centre">
	<iframe 
	src="../documents/refbacprociel.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_1e73f268d8f8278d69a83cdf6e68d18a.png){: .center width="800"}

## <div style="color: #fff;background-color: #B00862"> :loudspeaker: - Rénovation de la filière Systèmes Numériques en “ Cybersécurité, Informatique et réseaux, Electronique (CIEL) </div>
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_5edf00a63471cd1d7e37ab32484405fa.png){: .center width="800"}

### A - Activités professionnelles : 
Les <span style="color: #B00862;">activités professionnelles</span> exercées par le ou la titulaire du baccalauréat professionnel <span style="color: #B00862;">« Cybersécurité,Informatique et réseaux, Électronique »</span> sont : 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_55cfdc6c1e614cbb778b3e2ea7adad7d.png){: .center width="800"}

### B - Liste des compétences  : 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_06ca5941eabb4b33861ed91206e89021.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_e4ce8f6ec90a3c8879e441ae26394bc7.png){: .center width="800"}


:link: [Carte mentale des compétences en png](https://nuage03.apps.education.fr/index.php/s/iJM8dTxJdBPeT4E) et [en pdf](https://nuage03.apps.education.fr/index.php/s/snBmg9LbWpT453a)

:link: [Carte mentale sur Gitmind](https://www.google.com/url?q=https://gitmind.com/app/docs/m87abbyy&sa=D&source=editors&ust=1694873219008991&usg=AOvVaw17UR6QQpwioP62FbP9hEui)


### C - Tableau croisé Activités professionnelles / Compétences : 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_51994199f58eedfdf6ed125b35aed806.png){: .center width="800"}

---
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_07a2843ce344af88dcd26b4b9bd2cf27.png){: .center width="800"}

??? note "Les ressources d'EDUSCOL" 

    1. **[Vidéo de présentation](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/u20596/renovation_CIEL/6_Video_CIEL.mp4)**

    <iframe width="100%" height="400" src="https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/u20596renovation_CIEL/6_Video_CIEL.mp4" frameborder="0"></iframe>

    2. **[Flyer CIEL](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/actualites/15324/15324-2-flyer-ciel.pdf) et [Affiche CIEL](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/actualites/15324/15324-1-affiche-ciel-a4.pdf)** 


    ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_c8560a53c183fab18c1288483e4d99ed.png)

    3. **Liens utiles**

    * :link: - [BAC PRO CIEL - EDUSCOL ](https://eduscol.education.fr/sti/formations/bac-pro/bac-pro-cybersecurite-informatique-et-reseaux-electronique-ciel-rentree-2023)

    * :link: - [BAC PRO CIEL - Repères pour la formation ](https://docs.google.com/document/d/e/2PACX-1vSjY20WCsp79iwbSeaZi_Cddkceez0uUynBTvw_2-6J_AphxdiPycELhf4qrmQLpOuOR6psTM5QZBc1/pub)

    * :notebook: - [Référentiel BAC PRO CIEL](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/actualites/15324/15324-ref-bcp-ciel-vpub-eduscol.pdf) 
    * :link: - [01 - Introduction _ présentation de la filière](https://nuage03.apps.education.fr/index.php/s/Yt2XCyfPBcBsdd6) 
    * :link: - [02 - Place de l'électronique](https://nuage03.apps.education.fr/index.php/s/Wk32cfM4ypcjwcg)
    * :link: - [03 - Introduction de la cybersécurité](https://nuage03.apps.education.fr/index.php/s/MCfJ5NnR7E48JaT) 
    * :link: - [04 - CCF en Bac Pro et BTS](https://nuage03.apps.education.fr/index.php/s/WWYHBgmYn2wpwkD)   
   

## <div style="color: #fff;background-color: #B00862"> :computer: Enseignements professionnels </div>

###  <span style="color: #0869B0;">A - Stratégie pédagogique :</span> 

  * Une pédagogie de projet
  * Une approche par compétence 
  * Une proposition d'organisation des zones d’activités pour le baccalauréat professionnel selon les [repères pour la formation - BAC PRO CIEL - :link:](https://docs.google.com/document/d/e/2PACX-1vSjY20WCsp79iwbSeaZi_Cddkceez0uUynBTvw_2-6J_AphxdiPycELhf4qrmQLpOuOR6psTM5QZBc1/pub)

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_c5f392bf29f936adf5f3514e9864f525.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a258985591a56ca0def391c7cff41df8.png){: .center width="800"}


###  <span style="color: #0869B0;">B - Cybersécurité :</span>
* Sensibiliser et former à l'hygiène informatique personnelle
* Protéger les réseaux informatiques et industriels
* Valoriser la donnée dans la filière CIEL :
    
    * Valeur de la donnée
    * Chaîne d’acquisition, de stockage, de transmission et d’exploitation de la donnée
    * Les données applicatives
    * Les données en production : le cloud/Formation
    * Orchestration de la donnée
    * Sauvegarde et restauration des données

<span style="color: #B00862;">**Le baccalauréat professionnel CIEL :**</span>

Le niveau BAC PRO doit permettre de sensibiliser les apprenants aux menaces et dangers des attaques informatiques (virus informatiques, arnaques par courrier électronique et les logiciels malveillants,…).

A l’issue du BAC PRO, les apprenants seront en mesure  de :
* Reconnaître les signes de phishing, les courriels spam,...
* Éditer des mots de passes complexes
* Utiliser des VPN lors de certaines connexions.
* Sécuriser des réseaux de données 
* Utiliser des méthodes de cryptage des données
* Suivre des cybers attaques
* ...

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_1029fd9dbe17bd911a2e206327eeac8c.png){: .center width="800"}
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_9f67b3419ab9b12ffdfecbe87fb6fc78.png){: .center width="800"}

Les activités pédagogiques du pôle <span style="color: #B00862;"> « VALORISATION DE LA DONNÉE »</span>  permettent  de développer les compétences en lien avec la programmation, le codage et la résolution de problèmes, certains langages de programmation spécifiques de technologies émergentes, la sécurité informatique et une initiation à la conception de sites Web.

L’enseignement développé dans le pôle <span style="color: #B00862;"> « VALORISATION DE LA DONNÉE »</span> prends comme support des infrastructures ou parties d’infrastructures fonctionnelles, permettant des activités d’analyses des installations, de mise en service et d’intervention dans le cadre d’une maintenance préventive et/ou corrective (simple).

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_6a87bd7f5f8400366529ad1c0d2ba941.png){: .center width="800"}

 
###  <span style="color: #0869B0;">C - Informatique et Réseaux :</span> 

Les pôles d’activités « exploitation et maintenance de réseaux informatiques » pour le BAC PRO s’inscrivent dans le cadre de <span style="color: #B00862;">« l’Usine du Futur »</span>.

<span style="color: #B00862;">**Les réseaux du domaine tertiaire restent néanmoins dans le champ du bac professionnel CIEL.**</span>

Les Technologies de l’Information et de la Communication, accessibles au milieu industriel, ouvrent la voie à l’usine connectée et numérique. Elles permettent notamment :

* une communication continue, instantanée et intégrée d’informations relatives aux processus de de production (conception, fabrication, logistique et maintenance) ainsi qu’aux produits fabriqués,

* la simulation du produit, du procédé, du poste de travail et même de l’usine, de la logistique et de la chaîne de fournisseurs,

* l’autodiagnostic et l’auto adaptation des procédés et des équipements de production et le contrôle en continu des produits.

Les compétences développées dans la filière CIEL, s’appuient sur : 

* l’Internet des objets
* l’internet mobile
* les capteurs

###  <span style="color: #0869B0;">D - Electronique :</span> 
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_23cbc314d26fda0998f3d52d0664ebd6.png){: .center width="800"}

La formation dans la filière CIEL dans son pôle « Production et réparation de produits électroniques » vise à former des techniciens(nes) en électronique.

Ils ou elles participent ainsi à la conception, la production, la réalisation et la maintenance de cartes de systèmes, de sous-ensembles et produits électroniques.


![](https://minio.apps.education.fr/codimd-prod/uploads/upload_cb99501d7efddc14f065b3c52a505b4c.png){: .center width="800"}


En entreprise, les techniciens et techniciens supérieurs travaillent au sein d’une équipe, en étroite collaboration avec des ingénieurs, afin d’intégrer, de programmer, d’installer, de mettre en communication et de maintenir des équipements électroniques dans les domaines de la domotique, la robotique, les transports, l'aéronautique, l’audiovisuel la e-santé́, les objets connectés (IoT) et les industries.

Les formations de la filière CIEL visent à former des futurs technicien(ne)s en électronique, intervenant à des niveaux de responsabilité différents dans les domaines suivants :


* étude et conception,
* conduite d’installation,
* fabrication- production - intégration,
* essais et contrôle qualité
* maintenance et installation,
* relation client fournisseurs.
---
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_50b9bdec7792624543a29c63e8fe0dd5.png){: .center width="800"}

---

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_069c3dd924ee0a370ae5e0e834c27dc8.png){: .center width="800"}

---

## <div style="color: #fff;background-color: #B00862"> :loudspeaker: - Au sein du LP Louise Michel - BAC PRO CIEL </div>

### A - Lien des filiéres BAC PRO CIEL et BTS FED DBC

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_0b3036d41d4438090eba33f03bca9204.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_6f7a20bcd0a1ea2fc8881fdd14f89bf9.png){: .center width="800"}

### B - Organisation des activités (en cours d'élaboration) 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d0ad025e66d5de3256fcae69a42b801d.png){: .center width="800"}

### C - Organisation des plateaux techniques

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_e9c9628f57f0a2917a20784dedf3dd6b.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_17ef82bdd91d024053ca02dcf8358607.png){: .center width="800"}

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_3e900b80a6da6d2e0d8434ba8739f2de.png){: .center width="800"}

### D - Organisation du réseau pédagogique (en cours de modification et d'amélioration) 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_f4a583d111b1c270b035daf1c9243ab5.png){: .center width="800"}


**Equipe BAC PRO CIEL LP Louise Michel RUFFEC**

**CHAZEAUX Guillaume**
**CASTANEDA-NUNEZ Sébastien**
**ROUILLÉ Pierre**
**DUPUY Éric**
